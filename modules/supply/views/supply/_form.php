<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\modules\supply\models\Supply */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="supply-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provider_id')->widget(Select2::className(), [
        'name' => 'Provider ID',
        'data' => ArrayHelper::map($providers, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select provider--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'product_id')->widget(Select2::className(), [
        'name' => 'Product ID',
        'data' => ArrayHelper::map($products, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select product--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
