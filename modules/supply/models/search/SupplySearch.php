<?php

namespace app\modules\supply\models\search;

use app\modules\product\models\Product;
use app\modules\provider\models\Provider;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\supply\models\Supply;

/**
 * SupplySearch represents the model behind the search form of `app\modules\supply\models\Supply`.
 */
class SupplySearch extends Supply
{
    public $productName;
    public $providerName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date', 'productName', 'providerName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_product = Product::tableName();
        $tn_provider = Provider::tableName();
        $tn_supply = Supply::tableName();

        $query = Supply::find()
            ->from("$tn_supply as s")
            ->leftJoin("$tn_product as prod", 'prod.id = s.product_id')
            ->leftJoin("$tn_provider as prov", 'prov.id = s.provider_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['s.id' => SORT_ASC],
                    'desc' => ['s.id' => SORT_DESC],
                ],
                'productName' => [
                    'asc' => ['prod.name' => SORT_ASC],
                    'desc' => ['prod.name' => SORT_DESC],
                ],
                'providerName' => [
                    'asc' => ['prov.name' => SORT_ASC],
                    'desc' => ['prov.name' => SORT_DESC],
                ],
                'date' => [
                    'asc' => ['s.date' => SORT_ASC],
                    'desc' => ['s.date' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->productName)) {
            $query->andFilterWhere(['like', 'prod.name', $this->productName]);
        }

        if (!empty($this->providerName)) {
            $query->andFilterWhere(['like', 'prov.name', $this->providerName]);
        }

        $query->andFilterWhere([
            's.id' => $this->id,
            's.date' => $this->date,
        ]);

        return $dataProvider;
    }
}
