<?php

namespace app\modules\supply\models;

use Yii;
use app\modules\product\models\Product;
use app\modules\provider\models\Provider;

/**
 * This is the model class for table "{{%supply}}".
 *
 * @property int $id
 * @property int $provider_id
 * @property int $product_id
 * @property string $date
 *
 * @property Product $product
 * @property Provider $provider
 */
class Supply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%supply}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'product_id'], 'integer'],
            [['date'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provider::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    public function fields()
    {
        return [
            'productName',
            'providerName'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider_id' => 'Provider ID',
            'product_id' => 'Product ID',
            'productName' => 'For product name',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductName()
    {
        return ($this->product !== null) ? $this->product->name : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    public function getProviderName()
    {
        return ($this->provider !== null) ? $this->provider->name : null;
    }
}
