<?php

namespace app\modules\realization\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realization\models\Realization;
use app\modules\product\models\Product;

/**
 * RealizationSearch represents the model behind the search form of `app\modules\realization\models\Realization`.
 */
class RealizationSearch extends Realization
{
    public $productName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date', 'productName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_product = Product::tableName();
        $tn_realization = Realization::tableName();

        $query = Realization::find()
            ->from("$tn_realization as r")
            ->leftJoin("$tn_product as p", 'p.id = r.product_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['r.id' => SORT_ASC],
                    'desc' => ['r.id' => SORT_DESC],
                ],
                'productName' => [
                    'asc' => ['p.name' => SORT_ASC],
                    'desc' => ['p.name' => SORT_DESC],
                ],
                'date' => [
                    'asc' => ['r.date' => SORT_ASC],
                    'desc' => ['r.date' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->productName)) {
            $query->andFilterWhere(['like', 'p.name', $this->productName]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
        ]);

        return $dataProvider;
    }
}
