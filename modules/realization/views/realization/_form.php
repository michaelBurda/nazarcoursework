<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\realization\models\Realization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realization-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->widget(Select2::className(), [
        'name' => 'Product ID',
        'data' => ArrayHelper::map($products, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select product--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
