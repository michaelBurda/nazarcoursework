<?php

namespace app\modules\employee\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\employee\models\Employee;

/**
 * EmployeeSearch represents the model behind the search form of `app\modules\employee\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    private $fullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['last_name', 'first_name', 'middle_name', 'role', 'fullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_employee = Employee::tableName();

        $query = Employee::find()
            ->from("$tn_employee as e");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['e.id' => SORT_ASC],
                    'desc' => ['e.id' => SORT_DESC],
                ],
                'fullName' => [
                    'asc' => ['e.last_name' => SORT_ASC, 'e.first_name' => SORT_ASC, 'e.middle_name' => SORT_ASC],
                    'desc' => ['e.last_name' => SORT_DESC, 'e.first_name' => SORT_DESC, 'e.middle_name' => SORT_DESC],
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
                'role' => [
                    'asc' => ['e.role' => SORT_ASC],
                    'desc' => ['e.role' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->fullName)) {
            $query->andFilterWhere(['like', 'e.first_name', $this->fullName])
                ->orFilterWhere(['like', 'e.last_name', $this->fullName])
                ->orFilterWhere(['like', 'e.middle_name', $this->fullName]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'role', $this->role]);

        return $dataProvider;
    }
}
