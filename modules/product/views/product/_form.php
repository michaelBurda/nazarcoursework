<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'employee_id')->widget(Select2::className(), [
        'name' => 'Employee ID',
        'data' => ArrayHelper::map($employees, 'id', 'fullName'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select employee--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'date_creation')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
