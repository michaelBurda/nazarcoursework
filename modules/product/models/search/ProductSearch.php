<?php

namespace app\modules\product\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\product\models\Product;
use app\modules\employee\models\Employee;

/**
 * ProductSearch represents the model behind the search form of `app\modules\product\models\Product`.
 */
class ProductSearch extends Product
{
    public $employeeFullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'date_creation', 'employeeFullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_product = Product::tableName();
        $tn_employee = Employee::tableName();

        $query = Product::find()
            ->from("$tn_product as p")
            ->leftJoin("$tn_employee as e", 'e.id = p.employee_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['p.id' => SORT_ASC],
                    'desc' => ['p.id' => SORT_DESC],
                ],
                'name' => [
                    'asc' => ['p.name' => SORT_ASC],
                    'desc' => ['p.name' => SORT_DESC],
                ],
                'employeeFullName' => [
                    'asc' => ['e.last_name' => SORT_ASC, 'e.first_name' => SORT_ASC, 'e.middle_name' => SORT_ASC],
                    'desc' => ['e.last_name' => SORT_DESC, 'e.first_name' => SORT_DESC, 'e.middle_name' => SORT_DESC],
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
                'date_creation' => [
                    'asc' => ['p.date_creation' => SORT_ASC],
                    'desc' => ['p.date_creation' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->employeeFullName)) {
            $query->andFilterWhere(['like', 'e.first_name', $this->employeeFullName])
                ->orFilterWhere(['like', 'e.last_name', $this->employeeFullName])
                ->orFilterWhere(['like', 'e.middle_name', $this->employeeFullName]);
        }

        $query->andFilterWhere([
            'p.id' => $this->id,
            'p.date_creation' => $this->date_creation,
        ]);

        $query->andFilterWhere(['like', 'p.name', $this->name]);

        return $dataProvider;
    }
}
