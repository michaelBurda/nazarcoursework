<?php

namespace app\modules\product\models;

use Yii;
use app\modules\employee\models\Employee;
use app\modules\realization\models\Realization;
use app\modules\supply\models\Supply;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property string $name
 * @property int $employee_id
 * @property string $date_creation
 *
 * @property Employee $employee
 * @property Realization[] $realizations
 * @property Supply[] $supplies
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['date_creation'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    public function fields()
    {
        return [
            'employeeFullName',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'employee_id' => 'Employee ID',
            'date_creation' => 'Date Creation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    public function getEmployeeFullName()
    {
        return ($this->employee !== null) ? $this->employee->fullName : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealizations()
    {
        return $this->hasMany(Realization::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplies()
    {
        return $this->hasMany(Supply::className(), ['product_id' => 'id']);
    }
}
