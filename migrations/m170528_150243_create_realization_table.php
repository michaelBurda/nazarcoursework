<?php

use yii\db\Migration;

/**
 * Handles the creation of table `realization`.
 */
class m170528_150243_create_realization_table extends Migration
{
    private $tn_realization = '{{%realization}}';
    private $tn_product = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_realization, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11),
            'date' => $this->date()
        ]);

        $this->addForeignKey('FK_realization_product_id', $this->tn_realization, 'product_id', $this->tn_product, 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_realization_product_id', $this->tn_realization);

        $this->dropTable($this->tn_realization);
    }
}
