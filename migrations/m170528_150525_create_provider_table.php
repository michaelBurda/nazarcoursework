<?php

use yii\db\Migration;

/**
 * Handles the creation of table `provider`.
 */
class m170528_150525_create_provider_table extends Migration
{
    private $tn_provider = '{{%provider}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_provider, [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tn_provider);
    }
}
