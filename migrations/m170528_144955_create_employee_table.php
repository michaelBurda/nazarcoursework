<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m170528_144955_create_employee_table extends Migration
{
    private $tn_employee = '{{%employee}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_employee, [
            'id'            => $this->primaryKey(),
            'last_name'     => $this->string(30),
            'first_name'    => $this->string(30),
            'middle_name'   => $this->string(30),
            'role'          => $this->string(80)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tn_employee);
    }
}
