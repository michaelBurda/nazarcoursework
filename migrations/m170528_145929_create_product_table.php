<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170528_145929_create_product_table extends Migration
{
    private $tn_employee = '{{%employee}}';
    private $tn_product = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_product, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'employee_id' => $this->integer(11),
            'date_creation' => $this->date()
        ]);

        $this->addForeignKey('FK_product_employee_id', $this->tn_product, 'employee_id', $this->tn_employee, 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_product_employee_id', $this->tn_product);

        $this->dropTable($this->tn_product);
    }
}
