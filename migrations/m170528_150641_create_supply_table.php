<?php

use yii\db\Migration;

/**
 * Handles the creation of table `supply`.
 */
class m170528_150641_create_supply_table extends Migration
{
    private $tn_supply = '{{%supply}}';
    private $tn_product = '{{%product}}';
    private $tn_provider = '{{%provider}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_supply, [
            'id' => $this->primaryKey(),
            'provider_id' => $this->integer(11),
            'product_id' => $this->integer(11),
            'date' => $this->date()
        ]);

        $this->addForeignKey('FK_supply_provider_id', $this->tn_supply, 'provider_id', $this->tn_provider, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_supply_product_id', $this->tn_supply, 'product_id', $this->tn_product, 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_supply_provider_id', $this->tn_supply);
        $this->dropForeignKey('FK_supply_product_id', $this->tn_supply);

        $this->dropTable($this->tn_supply);
    }
}
